<?php 
  error_reporting(E_ALL);
  ini_set('display_errors', '1');
  session_start();
  require_once "./libs/vendor/autoload.php";
  require_once "./php/printtable.php";
  require_once "./php/initquery.php";
  require_once "./php/makeform.php";
  require_once "./php/sparql2csv.php";
  require_once "./php/requesttoquery.php";
  require_once "./php/hashsessionid.php";
  $session_id = hashsessionid(session_id());
  \EasyRdf\RdfNamespace::set('ab', 'http://localhost:3030/ds/localhost/testns#');
  $sparqlurl = 'http://localhost:3030/'.$session_id.'/query';
  $sparql = new \EasyRdf\Sparql\Client($sparqlurl);
?>

<!DOCTYPE html>
<html>
  <head>
    <?php include "./html/import.html"; ?>
   <script>
let isLeftDragging = false;
let isRightDragging = false;

function ResetColumnSizes() {
  // when page resizes return to default col sizes
  let page = document.getElementById("page");
  page.style.gridTemplateColumns = "1fr 6px 3fr";
}

function SetCursor(cursor) {
  let page = document.getElementById("page");
  page.style.cursor = cursor;
}

function StartLeftDrag() {
  console.log("mouse down");
  isLeftDragging = true;

  SetCursor("ew-resize");
}

function EndDrag() {
  console.log("mouse up");
  isLeftDragging = false;
  isRightDragging = false;

  SetCursor("auto");
}

function OnDrag(event) {
  if (isLeftDragging) {
    console.log("draggin!")
    let page = document.getElementById("page");
    let leftcol = document.getElementById("leftcol");

    let leftColWidth = isLeftDragging ? event.clientX : leftcol.clientWidth;

    let dragbarWidth = 6;

    let cols = [
      leftColWidth,
      dragbarWidth,
      page.clientWidth - dragbarWidth,
    ];

    let newColDefn = cols.map(c => c.toString() + "px").join(" ");

    console.log(newColDefn);
    page.style.gridTemplateColumns = newColDefn;

    event.preventDefault()
  }
}
   </script>
  </head>
  <body onresize="ResetColumnSizes()">
      <div id="page" onmouseup="EndDrag()" onmousemove="OnDrag(event)">
         <div id="leftcol">
            <div id="prompt">
               <?php include "./html/form.html"; ?>
            </div>
            <div id="table">
              <?php
                $query = isset($_POST['query']) ? $_POST['query'] : "reset";
                $sparqlQuery = requesttosparql($query);
                $result = $sparql->query($sparqlQuery);
                printtable($result);
                sparql2csv($result, $session_id);
                $x = shell_exec(escapeshellcmd("./python/makegraph.py ".$session_id));
              ?>
            </div>
         </div>
      <div id="leftdragbar" onmousedown="StartLeftDrag()"></div>
      <div id="graph" >
         <div  id="mynetwork" />
      </div>
    </div>
  </body>
</html> 
