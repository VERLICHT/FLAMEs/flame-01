<?php
  error_reporting(E_ALL);
  ini_set('display_errors', '1');
  require_once "./php/hashsessionid.php";  
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="../../css/style.css">  
    <link rel="stylesheet" href="./css/style.css">  
    <link rel="icon" href="../../img/kanarie.png">  
  </head>
<body>
  <h3>FLAME-01: netwerk analyse van een interview</h3>
  <p>
    Upload een gecodeerd interview. De app laat vervolgens een tabel en een 
    netwerk zien waartussen heen en weer geklikt kan worden. In de tabel kan 
    overal behalve op de laatste kolom geklikt worden en in het netwerk
    moet op de pijl worden geklikt. In het 
    zoekvakje boven, kan op codefragmenten worden gezocht. De app is alleen 
    geschikt voor (grote) computerschermen.
  </p>
  <p>
    Achter de schermen wordt het interview omgezet in zogenaamde triples
    in the turtle notatie (.ttl). De resulterende database wordt ingeladen 
    in Apache fuseki, wat querying op de data toelaat. De triples worden ook
    in de tabel en in het netwerk weergegeven.
  </p>
    <label for="images" class="drop-container" id="dropcontainer">
     <span class="drop-title">Sleep files hierin</span>
     <br>of<br>
     <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
       <input type="file" name="interview" id="interview"> <br>
       <p>
         <input id="upload_button" type="submit" value="Upload" name="submit">
       </p>
     </form>
   </label>
 
   <script>
     const dropContainer = document.getElementById("dropcontainer")
     const fileInput = document.getElementById("interview")
 
     dropContainer.addEventListener("dragover", (e) => {
       // prevent default to allow drop
       e.preventDefault()
     }, false)
 
     dropContainer.addEventListener("dragenter", () => {
       dropContainer.classList.add("drag-active")
     })
 
     dropContainer.addEventListener("dragleave", () => {
       dropContainer.classList.remove("drag-active")
     })
 
     dropContainer.addEventListener("drop", (e) => {
       e.preventDefault()
       dropContainer.classList.remove("drag-active")
       fileInput.files = e.dataTransfer.files
     })
 
   </script>

<?php
  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //set session id
    session_start();
    $session_id = hashsessionid(session_id()); //hash it so it doesn't end up in the repository

    $target_dir = "interviews/";
    //the file will be called sessionid.txt
    $target_file = $target_dir . $session_id . ".txt";
    $uploadOk = 1;
    $uploadFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Check if file already exists
    if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      //  $uploadOk = 0;
   }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
    }

    // Allow certain file formats
    if($uploadFileType != "txt") {
      echo "you uploaded a file of type: ".$uploadFileType;
      echo "Sorry, only plain text files are allowed.";
      $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES["interview"]["tmp_name"], $target_file)) {
        echo "The file ". htmlspecialchars( basename( $_FILES["interview"]["name"])). " has been uploaded.";
      } else {
        echo "Sorry, there was an error uploading your file.";
      }
    }
    //create the ttl and html files for the interview
    $x = shell_exec(escapeshellcmd("./python/processInterview.py interviews/".$session_id.".txt"));

    //delete the old data from the database
    $fuseki_delDB = shell_exec("curl 'http://localhost:3030/$/datasets/".$session_id."' -X DELETE");

    $command = "curl 'http://127.0.0.1:3030/$/datasets' -X POST --data-raw 'dbName=".$session_id."&dbType=tdb2'";
    $fuseki_makeDB = shell_exec($command);
    echo "Error: ".$fuseki_makeDB;
    $enterttl = 'curl --location --request POST "http://localhost:3030/'.$session_id.'/data" --header "Content-Type: multipart/form-data" --form "file=@./interviews/'.$session_id.'.ttl"';
    $fuseki_enterttl = shell_exec($enterttl);
    echo "Error: ".$fuseki_enterttl;

    header("Location:app.php");
    exit; // <- don't forget this!

    delete_everything();
  }
?>
</body>
</html> 
