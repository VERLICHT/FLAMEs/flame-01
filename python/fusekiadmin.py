#! /usr/bin/python3

from urllib import request
import json
import argparse

def listdatabases(printDB = False):
    r = request.Request('http://localhost:3030/$/server')
    r.add_header('Accept', 'application/json')
    response = request.urlopen(r)
    data = response.read().decode('utf-8')
    js = json.loads(data)

    alldb = [ds['ds.name'][1:] for ds in js['datasets']]
    if printDB: print('\n'.join(alldb))
    return alldb


def deletedatabase(name):
    if name not in listdatabases():
        print("database {} not found".format(name))
        return 0
    url = "http://localhost:3030/$/datasets/" + name
    r = request.Request(url)
    r.get_method = lambda: "DELETE"
    response = request.urlopen(r)
    return 1


def deletedatabases(dblist):
    for db in dblist:
        deletedatabase(db)


def main():
    parser = argparse.ArgumentParser(description='Interact with fuseki via http')
    parser.add_argument('--list',
                        '-l',
                        dest='list',
                        action='store_true',
                        help='List databases',
                        )
    parser.add_argument('--delete',
                        '-d',
                        dest='delete', 
                        help='Delete a database by providing an id, use . to delete all databases')

    args = parser.parse_args()


    if args.delete == '.':
        deletedatabases(listdatabases(args.list))
        return

    elif args.delete: #and id must have been provided
        deletedatabases([args.delete])
        return

    if args.list:
        listdatabases(True)
        return

main()
