#! /usr/bin/python3

import sys
import os
import re
from collections import namedtuple
import html


#TODO if we want to add codes under the statement instead of next to it:
# collect all Utterance objects in a list
# if a lone code is detected we add it to the previous object
# write the HTML and TTL on all of them once collected.

htmlHeader = """
<!DOCTYPE html>
<html>

<head>
<link rel="icon" href="../img/kanarie.png">
<link rel="stylesheet" href="../css/style.css">
<style>
html * {
 font-family:Courier New;
 font-size:10pt;
 }
.statement {
  padding-left:10px;
  }
</style>
</head>

<body>
<table>
"""

htmlFooter = """
</table>
</body>
</html>
"""



class Utterance(object):
    def __init__(self, line):
        self.ID = self.getID(line)
        self.statement = self.getStatement(line)
        self.codes = self.getCodes(line)
        self.none2emptystring()

    def none2emptystring(self):
        for elem in [self.ID, self.statement]:
            if not elem:
                elem = ""

    def getID(self, line):
        match = re.search("\[\[uid\=(.+?)\]\]", line)
        if match:
            return match.group(1)


    def getStatement(self, line):
        match =  re.search("\[\[uid\=.+?\]\](.+?)$",line)
        if match:
            ret = match.group(1) #could contain also codes
            if '[[' in ret:
                return ret[:ret.find('[[')]
            else:
                return ret
        else:
            return ""


    def getCodes(self, line):
        Triple = namedtuple('Triple', ['start', 'eind', 'type'])
        pattern = re.compile("\[\[(\w+?)\-\>(\w+?)\|(?:\||\>)(\w+?)(?:\|\||\]\])")
        codes = re.findall(pattern, line)
        print([Triple(*code) for code in codes])
        return [Triple(*code) for code in codes]


    def codeToTTL(self, code, namespace, counter):
        link = namespace + '_' + str(counter + 1)
        ofString = ""
        if counter == 0:
            ofString += "\n" #create space between multiple codes
        for field in code._fields:
            ofString += 'ab:{} ab:{} "{}" . \n'.format(link, field, getattr(code, field))
        ofString += 'ab:{} ab:{} "{}" . \n'.format(link, "link", link)
        return ofString


    def writeTTL(self, ofHandle):
        ofStrings = [] 
        print(os.path.splitext(os.path.basename(ofHandle.name))[0])
        print(self.ID)
        namespace = os.path.splitext(os.path.basename(ofHandle.name))[0] + '_' + self.ID
        for counter, code in enumerate(self.codes): 
            ofStrings.append(self.codeToTTL(code, namespace, counter))
        return '\n'.join(ofStrings) 


    def codesToHTML(self):
        ret = ""
        for code in self.codes:
            ret += "[[{} -> {} || {}]]".format(code.start, code.eind, code.type)
        return ret


    def writeHTML(self):
        #TODO add time
        print(self.statement, self.ID, self.codes)
        ofString = """
<tr id='{id}'>
  <td>[[uid={id}]]</td> 
  <td class="statement"><span style="white-space: nowrap;">{statement}</span></td>
  <td class="codes"><span style="white-space: nowrap;">{codes}</span></td>
</tr>""".format(id=html.escape(self.ID), statement
                = html.escape(self.statement),
                              codes = html.escape(self.codesToHTML()))
        return ofString


def openOfStreams(fileName):
    fileNameRoot = fileName.split(".")[0]
    fileNameHTML = fileNameRoot + ".html"
    fileNameTTL = fileNameRoot + ".ttl"
    ofStreamHTML = open(os.path.abspath(os.getcwd()) + "/" + fileNameHTML, 'w')
    ofStreamTTL = open(os.path.abspath(os.getcwd()) + "/" + fileNameTTL, 'w')
    return ofStreamHTML, ofStreamTTL


def processFile(fileName):
    ofStreamHTML, ofStreamTTL = openOfStreams(fileName)
    ofStreamHTML.writelines(htmlHeader)
    TTLHEADER = "@prefix ab: <http://localhost:3030/ds/localhost/testns#> .\n\n"
    ofStreamTTL.writelines(TTLHEADER)

    with open(os.path.abspath(os.getcwd()) + "/" + fileName) as fileHandle:
        for line in fileHandle:
            line = line.strip()
            if line:
                utterance = Utterance(line)
                ofStreamHTML.writelines(utterance.writeHTML())
                ofStreamTTL.writelines(utterance.writeTTL(ofStreamTTL))

            else:
                ofStreamHTML.writelines(["<br>"])
    ofStreamHTML.writelines(htmlFooter)

def help():
    return """
    This script processes an interview into HTML and TTL.
    Options:
     -h for help
     <filename> to process one interviews
     *  to process all interviews

     Output files with have the same filename, but with a different extensions.
     Existing files will be overwritten.
    """


def isTextFile(fn):
    return 'txt' in os.path.splitext(os.path.basename(fn))[1]


def getTextFiles(files):
    textfiles = filter(isTextFile, files)
    return textfiles


if __name__ == "__main__":
    #handle arguments *, fn, multiple fn
    arguments = sys.argv
    if len(arguments) ==1:
        print("No sensible option was provided. Please use '-h' for help.")

    elif "-h" in arguments:
        print(help())   

    else:
        textfiles = list(getTextFiles(arguments[1:]))
        if textfiles: 
            for fileName in textfiles:
                processFile(fileName)
        else:
            print("No textfiles found.")

       
            
        
