#! /usr/bin/python3
import csv
import sys


class Graph(object):
    def __init__(self):
        #set with names of just: [node1, node2, ...]
        self.nodes = set()
        #dict of tuples [(node1, node2):set(labels)] to connect them
        self.edges = dict()


    def readGraphUpdate(self, fn):
        with open(fn, 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                triple = ['"{}"'.format(x.strip()) for x in row][0:3]
                self.addTriple(*triple)


    def addTriple(self, startNode, endNode, rel):
        self.nodes.add(startNode)
        self.nodes.add(endNode)
        self.edges.setdefault((startNode, endNode), set()).add(rel)


    def graph2visjs(self, of):
        f = open(of, 'w') 
        with open("./python/makegraph.js",'r') as inf:
            for line in inf:
                #find comment //printnodeshere or //printedgeshere
                if "//print" in line:
                    #infer the indentation level
                    indent = ' ' * (len(line) - len(line.lstrip(' ')))
                    if "nodes" in line:
                        f.write(self.printNodesJS(indent))
                    elif "edges" in line:
                        f.write(self.printEdgesJS(indent))
                else:
                    f.write(line)
        f.close()


    def printNodesJS(self, indent):
        ret = ""
        for ID, label in enumerate(self.nodes):
            ret += "{}{{id: {}, label: {}}},\n".format(indent, label, label)
        return ret


    def printEdgesJS(self, indent):
        ret = ""
        edgeLine = "{}{{from: {}, to: {}, id: {}, label: {}, arrows: 'to'}},\n"
        i = 1
        for nodes, labels in self.edges.items():
            for label in labels:
                ret += edgeLine.format(indent, nodes[0], nodes[1], i, label)
                i += 1

        return ret


if __name__ == "__main__":
    g = Graph()
    ID = sys.argv[1] #this should be the session ID supplied by PHP
    g.readGraphUpdate("./tmp/{}_queryoutput.txt".format(ID))
    g.graph2visjs("./tmp/{}_graph.js".format(ID))



        

