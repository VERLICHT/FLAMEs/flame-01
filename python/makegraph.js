var nodes;
var edges;
var network;
 
document.addEventListener('DOMContentLoaded', function () {
    // create an array with nodes 
    nodes = new vis.DataSet([
        //printnodeshere
    ]);

    edges = new vis.DataSet([
        //printedgeshere
    ]);    

    // create a network
    var container = document.getElementById("mynetwork");
    var data = {nodes: nodes, edges: edges,};

    var options = {
        layout: {
            randomSeed: 1337,
            improvedLayout: true,
        },
        edges: {
           color : {
              inherit: false
           }
        },
        physics: {
            barnesHut: 
            {
               gravitationalConstant: -3000,
               springLength: 200,
               springConstant: 0.01,
//               avoidOverlap:1,  #als je dit aanzet, stabiliseert het netwerk niet
            },
            repulsion: {
                centralGravity: 0.2,
                springLength: 200,
                springConstant: 0.05,
                nodeDistance: 100,
                damping: 0.09
            },
            maxVelocity: 50,
            solver: "barnesHut",
            timestep: 0.35,
            stabilization: true
        },
        interaction: 
        {
            tooltipDelay: 200,
            hideEdgesOnDrag: true,
        }
    };
    network = new vis.Network(container, data, options);

    //make edges clickable
    var selectedTriple
    network.on("click", function (params) {
       json = JSON.stringify(params, null, 4)
  
       edgeId = params.items[0].edgeId
       selNodes = network.getConnectedNodes(edgeId)
       clickedLabel = edges.get(edgeId).label.trim()
       node1 = selNodes[0].trim()
       node2 = selNodes[1].trim()
       id = node1+'-'+node2+'-'+clickedLabel
       if (typeof selectedTriple !== 'undefined') {
           selectedTriple.style.color = "black"
           selectedTriple.style.fontWeight = "normal"
    }
    //highlight corresponding row in table and scroll to it
    selectedTriple = document.getElementById(id)
    selectedTriple.scrollIntoView();
    selectedTriple.style.color = "red"
    selectedTriple.style.fontWeight = "bold"
  })
});
