<script>
  $('#submitQuery').on('submit', function () {
      var start = document.getElementById("start").value
      var eind = document.getElementById("eind").value
      var relatie = document.getElementById("relatie").value
      var queryType;

      if (start){ queryType = "start"}
      if (eind){ queryType = "eind"}
      if (relatie){ queryType = "type"}
      var queryVal = start + eind + relatie
      var query = `
PREFIX ab: <http://localhost:3030/ds/localhost/testns#>

SELECT ?start ?eind ?type ?locatie
WHERE
{
?id ab:${queryType} "${queryVal}" ;
ab:start ?start ;
ab:eind ?eind ;
ab:type ?type;
ab:link ?locatie .
}
ORDER BY ?start ?eind ?type`

     document.getElementById("inputquery").value = query;
     document.getElementById('submitQuery').action = "app.php";
     document.getElementById("submitQuery").submit();
     return false;
    });
</script>
<script>
  $('#resetButton').on('submit', function () {
      var query = `
PREFIX ab: <http://localhost:3030/ds/localhost/testns#>

SELECT ?start ?eind ?type ?locatie
WHERE
{
?id ?x ?y" ;
ab:start ?start ;
ab:eind ?eind ;
ab:type ?type;
ab:link ?locatie .
}
ORDER BY ?start ?eind ?type`

     document.getElementById("inputquery").value = query;
     document.getElementById('submitQuery').action = "app.php";
     document.getElementById("submitQuery").submit();
     return false;
    });
</script>

