document.addEventListener("DOMContentLoaded", function(){
var oldTriple

function changeNodeColor(node, color)
{
  node.color = {border:color}
  nodes.update(node)
  return node
}

function changeEdgeColor(edge, color)
{
  edges.update({id:edge.id,color:{color:color}});
  return edge
}

function getEdgeBetweenNodes(node1, node2, label) 
{
  return edges.get().filter(function (edge)
  {
    return edge.from == node1.id && edge.to == node2.id && edge.label == label;
  })[0];
}

function getConnectedNode(node1, node2id)
{
  for (node of nodes.get(network.getConnectedNodes(node1.id)))
  {
    if (node.id.trim() == node2id) return node 
  }
}

function stringToTriple(tripleId)
{
  [n1, n2, e] = tripleId.split('-')
  node1 = nodes.get(n1)
  node2 = getConnectedNode(node1, n2)
  e = getEdgeBetweenNodes(node1, node2, e)
  return Array(node1, node2, e)
}

function changeTripleColor(triple, color, edgecolor = 0) 
{
  edgecolor = edgecolor ? edgecolor : color;
  [node1, node2, edge] = triple
  node1 = changeNodeColor(node1, color)
  node2 = changeNodeColor(node2, color)
  edge = changeEdgeColor(edge, edgecolor)
  return Array(node1, node2, edge)
}

allTableRows = document.querySelectorAll("tr")
for(row of allTableRows){
   row.addEventListener("click", function (event){
   tripleId = event.target.parentNode.id
   if (oldTriple)
   {
     changeTripleColor(oldTriple, "#2B7CE9", "#848484")
   }
   newTriple = stringToTriple(tripleId)
   oldTriple = changeTripleColor(newTriple, "red")
   network.focus(newTriple[0].id, {scale:1})
   })
}

})

