<?php


function initquery()
{

$query = <<<QQQ
PREFIX ab: <http://localhost:3030/ds/localhost/testns#>

SELECT ?start ?eind ?eind ?type ?locatie
WHERE
{
  ?id ab:type "causal_pos" ;
      ab:start ?start ;
      ab:eind ?eind ;
      ab:type ?type;
      ab:link ?locatie .
}
ORDER BY ?start ?eind ?type
QQQ;

return($query);
}

?>
