<?php

function queryString($type, $keyword)
{
$query = <<<QQQ
PREFIX ab: <http://localhost:3030/ds/localhost/testns#>

SELECT ?start ?eind ?type ?locatie
WHERE
{
?id ab:$type "$keyword" ;
ab:start ?start ;
ab:eind ?eind ;
ab:type ?type;
ab:link ?locatie .
}
ORDER BY ?start ?eind ?type
QQQ;

return $query;
}

function resetGraph()
{
$reset = <<<QQQ
PREFIX ab: <http://localhost:3030/ds/localhost/testns#>

SELECT DISTINCT ?start ?eind ?type ?locatie
WHERE
{
?id ?x ?y ;
ab:start ?start ;
ab:eind ?eind ;
ab:type ?type;
ab:link ?locatie .
}
ORDER BY ?start ?eind ?type
QQQ;
return $reset;
}


function requesttosparql($post)
{
  if ($post == "reset")
  {
    return resetGraph();
  }
  else
  {
    $keywords = explode(';', $post );
    return queryString($keywords[0], $keywords[1]);
  }
} 
?>





























