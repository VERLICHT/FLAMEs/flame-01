<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once "loc2link.php";

function sparql2csv($result, $id, $class="tablesorter")
{
  $of = fopen("./tmp/".$id."_queryoutput.txt", "w") or die("test");
  foreach($result as $row)
  {
    foreach( $result->getFields() as $field )
    {
      $f = ($field == "locatie") ? loc2link($row->$field) : $row -> $field;
      fwrite($of, $f);
      fwrite($of, ", ");
    }
    fwrite($of, "\n");
  }
  fclose($of);
}

?>

