<?php
require_once "loc2link.php";
require_once "row2array.php";
require_once "makeid.php";
require_once "printtablerow.php";

function printtable($result, $class="tablesorter")
{
  print "<p>".$result->numRows()." resultaten gevonden: </p>";
  print "<table id='myTable' class='tablesorter'>\n";
  print "<thead>\n";
  print "<tr>\n";
  foreach( $result->getFields() as $field )
  {
    print "<th>$field</th>";
  }
  print "</tr>\n";
  print "</thead>\n";
  print "<tbody>\n";
  if ($result->numRows() == 0)
  {
    echo "<tr><td>Geen data gevonden</td></tr>";
    echo "<tr><td>Controleer of het geuploade document correct is.</td></tr>";
    echo "<tr><td>Controleer of de opgevraagde term in het document voorkomt.</td></tr>";
    echo "</tbody>";
    echo "</table>";
    return;
  }


  $lineID = "";
  $first = 1;
  $links = "";
  $row = array();
  $i=0;
  foreach($result as $row)
  {
    $i++;
    $newFields = row2array($row, $result, $i);
    $newID = makeid($newFields);

    if ($first == 1)
    {
      $first=0;
      $fields = $newFields;
    }
    else if ($lineID == $newID) #encountered same triple
    {
      $fields[3] = $fields[3].$newFields[3];
    }
    else
    {
      printtablerow($fields, $lineID);
      $i=1;
      $fields = row2array($row, $result, $i);

    }
    $lineID=$newID;
  }
  printtablerow($fields, $lineID);
  print "</tbody>";
  print "</table>";
}

?>

