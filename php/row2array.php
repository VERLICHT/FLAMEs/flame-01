<?php
require_once "loc2link.php";

function row2array($row, $result, $i)
{
    $rowItems = array();
    foreach( $result->getFields() as $field )
    {
      $f = ($field == "locatie") ? loc2link($row->$field, $i) : (string)$row -> $field;
      array_push($rowItems, $f);
    }
    return($rowItems);
} 
?>
